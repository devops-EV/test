#!/bin/bash
docker-compose kill
echo "y" | docker-compose rm
#docker rmi mysql:5.7
#docker rmi node:alpine
#docker rmi alpine:3.7
docker rmi seeder
docker rmi backup-client
docker rmi backup-server
echo 'y' | docker image prune
echo 'y' | docker network prune
