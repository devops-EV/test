#!/bin/bash

# create table (ticks)
MYSQL_PWD=$MYSQL_PASSWORD mysql -h $MYSQL_SERVER -u $MYSQL_USER -D $MYSQL_DATABASE < sql_scripts/create_table.sql > /dev/null 2>&1 &&
echo 'Table has been successfully created'
