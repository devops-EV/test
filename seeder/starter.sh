#!/bin/bash

# check mysql is ready
sh ./check_db_status.sh 2> /dev/null &&

if [ $? -ne 0 ] 
then
  echo "Couldn't establish DB connection!\nrestarting..."
  exit 1
fi

# create table (ticks)
sh ./create_table.sh
if [ $? -ne 0 ] 
then
  echo "Can't create table!"
  exit 1
fi

sh ./seed_data.sh
