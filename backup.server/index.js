'use strict';

const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const shell = require('shelljs');
const fs = require('fs');
const port = process.env.PORT || 3000;

http.listen(port, function() {
//  console.log('listening on *:' + port);
  shell.exec('sh ./check_db_status.sh');
  shell.exec('mkdir backups &> /dev/null');
  var i = 0;
  var successful_random_count=Math.floor(Math.random() * 7);
  var timer = setInterval(function() {
    if ( i++ < successful_random_count) {
      console.log('Starting MySQL Backup');
      shell.exec('sh ./make_backup.sh');
      console.log('MySQL Backup finished successfully');
      var stats = fs.statSync("backups/ticks.sql.gz");
      var BACKUP_SIZE = stats["size"];
      var BACKUP_DATE_TIME = stats["mtime"];
      io.emit('channel1', ['Name: ticks.sql.gz',  'Size: ' + BACKUP_SIZE + 'B', 'Date: ' + BACKUP_DATE_TIME.toDateString() +
      ', ' + BACKUP_DATE_TIME.toTimeString().slice(0,8) ]);
    } else {
      i = 0;
      successful_random_count=Math.floor(Math.random() * 7);
      console.log('Starting MySQL Backup');
      console.log('MySQL Backup failed');
      var BACKUP_FAILED_DATE_TIME = new Date();
      io.emit('channel1', ['Backup failed on ' + BACKUP_FAILED_DATE_TIME.toDateString() + ', ' +
      BACKUP_FAILED_DATE_TIME.toTimeString().slice(0,8) ]);
    };
  }, 5000);
});
