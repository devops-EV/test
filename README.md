# This readme is for the test devops interview task

## OVERVIEW:

## Repository contains 4 services:

1. MySQL server with a database
2. Seeder service - an application/script that does the following: 
 - Connects to MySQL server
   Creates a table called “ticks” with 2 fields in it:
   id - primary unique key
   created_at - datetime field, that will contain a date of record creation

 - inserts data upon a schedule into the created table continuously.
   1 row per 3  seconds

3. Backup backend - an application/script that does the following:
 - Creates the “ticks” table dump, using mysqldump cli
 - Compresses the created dump file with gzip
 - Backup schedule - once in 5 seconds
 - Simulates backup failures at random
 - Implemented functionality for communication with a client (next section)

4. Backup client - Web application:
 - Receives notifications from backend service (via socket.io, that describe
 the backup attempts were successful or failed and contain the backup job details.
 - Displays notifications in a column, newest on top.
 - Notification displays the backup file name, file size, and creation date.

## Project Structure:
```
.
├── backup.client
│   ├── Dockerfile
│   ├── index.html
│   └── package.json
├── backup.server
│   ├── check_db_status.sh
│   ├── Dockerfile
│   ├── index.js
│   ├── make_backup.sh
│   └── package.json
├── db.env
├── destroy_environment.sh
├── docker-compose.yml
├── README.md
└── seeder
    ├── check_db_status.sh
    ├── create_table.sh
    ├── Dockerfile
    ├── seed_data.sh
    ├── sql_scripts
    │   ├── create_table.sql
    │   └── seed_data.sql
    └── starter.sh
```

## Comments on implementation:

1. MySQL DB:
*  There is no published port for local machine. If needed, docker-compose.yml
   should be adjusted
*  Connection data/credentials were specified in a dedicated "db.env" file
   in order not to duplicate the information in compose file.
   One issue here is root password is also exposed. For this situation - OK.
   In production other solution will be implemeted

2. Seeder service:
* was decided to do it simply on Linux image with bash scripts

3. Backup Server:
* opens socket and sends event to it
* random function is used to simulate backup failure

4. Backup Client:
* npm http-server is used here


## Improvements to be done:

* frontend definitely, apply some styles or even some framework
* store backup history in DB, for an example using redis
* apply container healthchecks, improve DB availability varification, up/down/restart policy
* 