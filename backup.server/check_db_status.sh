#!/bin/sh

echo 'Checking availability of mysql:3306'
attempts_count=0
until MYSQL_PWD=$MYSQL_PASSWORD mysqladmin -h $MYSQL_SERVER -u $MYSQL_USER processlist> /dev/null; do
  echo 'Host mysql:3306 is not available yet'
  attempts_count=$((attempts_count+1))
  if [ "$attempts_count" -eq "50" ]; then
    exit 1
  fi
  sleep 10
done

echo 'Connected to mysql:3306'
