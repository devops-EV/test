#!/bin/bash

# seed data to the table (ticks) every 3 seconds
echo 'seeding data...'
while :
do
  MYSQL_PWD=$MYSQL_PASSWORD mysql -h $MYSQL_SERVER -u $MYSQL_USER -D $MYSQL_DATABASE < sql_scripts/seed_data.sql > /dev/null 2>&1 &&
  sleep 3
done
